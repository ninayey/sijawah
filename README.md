# SIJAWAH

_Objective_: Menjadikan PT Jawah Sahabat Sehat Indonesia mengelola proses antar lini bisnis yang berhubungan dengan
pelanggan lebih terintegrasi sehingga memudahkan proses bisnis PT Jawah Sahabat Sehat Indonesia.

## Kelompok NewJeans (B05)

* **Siti Fatimah Tuma'ninah** - *2006596743*
* **Bintang Gabriel Hutabarat** - *2006596661*
* **Farhan Hanif Saefuddin** - *2006596642*
* **Helga Syahda Elmira** - *2006463686*
* **Kelvin Erlangga** - *2006596964*

## *Jumlah Fitur*
SIJAWAH memiliki 28 fitur yang berfokus pada pengelolaan efisiensi manajemen pelanggan, pekerjaan, rapat, dan ide konten media sosial.
Test deployment : https://propensi-b05.up.railway.app/
