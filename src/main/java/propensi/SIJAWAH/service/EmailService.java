package propensi.SIJAWAH.service;

import propensi.SIJAWAH.model.RapatModel;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public interface EmailService {
    void sendMail(String state, List<String> emailList, String feedback, RapatModel rapat);

    String usernameEmailExtractor(String email);

    String dateTimeFormatter(LocalDateTime time);
}
