package propensi.SIJAWAH.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import propensi.SIJAWAH.model.RapatModel;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired private JavaMailSender javaMailSender;
    @Override
    public void sendMail(String state, List<String> emailList, String feedback, RapatModel rapat){
        SimpleMailMessage mailMessage
                = new SimpleMailMessage();

        // Setting up necessary details
        String newLine = " \n";
        String newLineDouble = "\n\n";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        mailMessage.setFrom("sijawahofficial@gmail.com");
        for (String email : emailList){
            String username = usernameEmailExtractor(email);
            mailMessage.setTo(email);
            if (state.equals("new")){
                mailMessage.setSubject("New Meeting");
                String text = "Hi, "+ username +" ! Anda telah diundang pada rapat dengan detail berikut " +newLineDouble +
                        "Nama Rapat : " + rapat.getNama() + newLine +
                        "PIC Rapat : " + rapat.getPic().getNama()+ newLine +
                        "Waktu : " + rapat.getWaktu().format(dateTimeFormatter) + newLine +
                        "Link : " + rapat.getLink() + newLineDouble +
                        "Informasi lain dapat Anda lihat pada laman Sijawah" + newLineDouble +
                        "Terima kasih," + newLine +
                        "Have a nice day!";
                mailMessage.setText(text);
            } else if (state.equals("cancel")){
                mailMessage.setSubject("Canceled Meeting");
                String text = "Hi, "+ username +" ! Rapat dengan detail berikut: " +newLineDouble +
                        "Nama Rapat : " + rapat.getNama() + newLine +
                        "PIC Rapat : " + rapat.getPic().getNama()+ newLine +
                        "Waktu : " + rapat.getWaktu().format(dateTimeFormatter) + newLine +
                        "Link : " + rapat.getLink() + newLineDouble +
                        "telah dibatalkan oleh CEO dengan alasan sebagai berikut: " + newLine +
                        "'" + feedback + "'" + newLineDouble +
                        "Informasi lain dapat Anda lihat pada laman Sijawah" + newLineDouble +
                        "Terima kasih," + newLine +
                        "Have a nice day!";
                mailMessage.setText(text);
            } else if (state.equals("update")){
                mailMessage.setSubject("Updated Meeting");
                String text = "Hi, "+ username +" ! Rapat yang dilaksananakan pada "+ rapat.getWaktu().format(dateTimeFormatter) + " telah di upate dengan detail berikut: " +newLineDouble +
                        "Nama Rapat : " + rapat.getNama() + newLine +
                        "PIC Rapat : " + rapat.getPic().getNama()+ newLine +
                        "Waktu : " + rapat.getWaktu().format(dateTimeFormatter) + newLine +
                        "Link : " + rapat.getLink() + newLineDouble +
                        "Informasi lain dapat Anda lihat pada laman Sijawah" + newLineDouble +
                        "Terima kasih," + newLine +
                        "Have a nice day!";
                mailMessage.setText(text);
            } else {
                mailMessage.setSubject("New Meeting");
                String text = "Hi, "+ username +" ! Anda telah diundang pada rapat dengan detail berikut " +newLineDouble +
                        "Nama Rapat : " + rapat.getNama() + newLine +
                        "PIC Rapat : " + feedback + newLine +
                        "Waktu : " + rapat.getWaktu().format(dateTimeFormatter) + newLine +
                        "Link : " + rapat.getLink() + newLineDouble +
                        "Informasi lain dapat Anda lihat pada laman Sijawah" + newLineDouble +
                        "Terima kasih," + newLine +
                        "Have a nice day!";
                mailMessage.setText(text);
            }

            // Sending the mail
            javaMailSender.send(mailMessage);
        }
    }

    @Override
    public String usernameEmailExtractor(String email){
        int indexAt = email.indexOf("@");
        return email.substring(0,indexAt);
    }

    @Override
    public String dateTimeFormatter(LocalDateTime time){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String formattedTime = time.format(dateTimeFormatter);
        return formattedTime;
    }
}
